import * as http from 'http';

const server = http.createServer((req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('Hello world from ts, nodemon and concurrently');
    res.end()
});

server.listen(3000, () => console.log(`server listening on port ${3000}`));